package plugins.danyfel80.randomcrops;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import danyfel80.randomcrops.RandomRectangleGenerator;
import icy.roi.ROI2D;
import icy.sequence.Sequence;
import icy.sequence.SequenceUtil;
import icy.system.IcyHandledException;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.ezplug.EzGroup;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzStoppable;
import plugins.adufour.ezplug.EzVarBoolean;
import plugins.adufour.ezplug.EzVarInteger;
import plugins.adufour.ezplug.EzVarSequence;
import plugins.adufour.vars.lang.VarSequenceArray;
import plugins.kernel.roi.roi2d.ROI2DRectangle;

public class GenerateRandomCrops extends EzPlug implements Block, EzStoppable
{

    private EzVarSequence varInSequence;
    private EzVarInteger varInRectangleWidth;
    private EzVarInteger varInRectangleHeight;
    private EzVarInteger varInNumberOfRectangles;
    private EzVarBoolean varInLimitRectanglesToROIs;

    @Override
    protected void initialize()
    {
        initializeInputVars();
        addEzComponent(varInSequence);
        addEzComponent(new EzGroup("Generated rectangles", varInRectangleWidth, varInRectangleHeight,
                varInNumberOfRectangles));
        addEzComponent(varInLimitRectanglesToROIs);
    }

    private void initializeInputVars()
    {
        varInSequence = new EzVarSequence("Sequence");
        varInRectangleWidth = new EzVarInteger("Rectangle width (px)", 512, 1, 10000, 1);
        varInRectangleHeight = new EzVarInteger("Rectangle height (px)", 512, 1, 10000, 1);
        varInNumberOfRectangles = new EzVarInteger("Number of rectangles", 10, 0, 10000000, 1);
        varInLimitRectanglesToROIs = new EzVarBoolean("Limit rectangles to ROIs", false);
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        initializeInputVars();
        inputMap.add(varInSequence.name, varInSequence.getVariable());
        inputMap.add(varInRectangleWidth.name, varInRectangleWidth.getVariable());
        inputMap.add(varInRectangleHeight.name, varInRectangleHeight.getVariable());
        inputMap.add(varInNumberOfRectangles.name, varInNumberOfRectangles.getVariable());
        inputMap.add(varInLimitRectanglesToROIs.name, varInLimitRectanglesToROIs.getVariable());
    }

    private VarSequenceArray varOutSequences;

    @Override
    public void declareOutput(VarList outputMap)
    {
        varOutSequences = new VarSequenceArray("Random crops");
        outputMap.add(varOutSequences.getName(), varOutSequences);
    }

    @Override
    protected void execute()
    {
        readParameters();
        try
        {
            generateRandomRectangles();
        }
        catch (Exception e)
        {
            throw new IcyHandledException("Error generating random rectangles: " + e.getMessage(), e);
        }
        setOutput();
    }

    private Sequence sequence;
    private Dimension rectangleSize;
    private Integer rectangleCount;
    private Boolean limitRectanglesToRois;
    private List<ROI2D> limitingRois;

    private void readParameters()
    {
        sequence = varInSequence.getValue(true);
        rectangleSize = new Dimension(varInRectangleWidth.getValue(), varInRectangleHeight.getValue());
        rectangleCount = varInNumberOfRectangles.getValue();
        limitRectanglesToRois = varInLimitRectanglesToROIs.getValue();
        limitingRois = new ArrayList<>();
        List<ROI2D> sequenceRois = sequence.getROI2Ds();
        if (!limitRectanglesToRois || sequenceRois.isEmpty())
        {
            limitingRois.add(new ROI2DRectangle(new Rectangle(sequence.getWidth(), sequence.getHeight())));
        }
        else
        {

            limitingRois.addAll(sequenceRois);
        }
    }

    private List<Sequence> croppedSequences;

    private void generateRandomRectangles() throws Exception
    {
        RandomRectangleGenerator rectGenerator = new RandomRectangleGenerator.Builder(rectangleSize, rectangleCount,
                limitingRois).build();
        List<ROI2DRectangle> rectangles = rectGenerator.call();
        croppedSequences = rectangles.stream().map(roi -> SequenceUtil.getSubSequence(sequence, roi))
                .collect(Collectors.toList());
    }

    private void setOutput()
    {
        if (isHeadLess())
        {
            varOutSequences.setValue(croppedSequences.stream().toArray(Sequence[]::new));
        }
        else

        {
            croppedSequences.stream().forEach(seq -> addSequence(seq));
        }
    }

    @Override
    public void clean()
    {// Nothing to do here
    }

}
