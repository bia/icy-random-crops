package plugins.danyfel80.randomcrops;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import danyfel80.randomcrops.RandomRectangleGenerator;
import icy.roi.ROI;
import icy.roi.ROI2D;
import icy.sequence.Sequence;
import icy.system.IcyHandledException;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.ezplug.EzGroup;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzVarBoolean;
import plugins.adufour.ezplug.EzVarInteger;
import plugins.adufour.ezplug.EzVarSequence;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.kernel.roi.roi2d.ROI2DRectangle;

public class GenerateRandomRectangleRois extends EzPlug implements Block
{

    private EzVarSequence varInSequence;
    private EzVarInteger varInRectangleWidth;
    private EzVarInteger varInRectangleHeight;
    private EzVarInteger varInNumberOfRectangles;
    private EzVarBoolean varInLimitRectanglesToROIs;

    @Override
    protected void initialize()
    {
        initializeInputVars();
        addEzComponent(varInSequence);
        addEzComponent(new EzGroup("Generated rectangles", varInRectangleWidth, varInRectangleHeight,
                varInNumberOfRectangles));
        addEzComponent(varInLimitRectanglesToROIs);
    }

    private void initializeInputVars()
    {
        varInSequence = new EzVarSequence("Sequence");
        varInRectangleWidth = new EzVarInteger("Rectangle width (px)", 512, 1, 10000, 1);
        varInRectangleHeight = new EzVarInteger("Rectangle height (px)", 512, 1, 10000, 1);
        varInNumberOfRectangles = new EzVarInteger("Number of rectangles", 10, 0, 10000000, 1);
        varInLimitRectanglesToROIs = new EzVarBoolean("Limit rectangles to ROIs", false);
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        initializeInputVars();
        inputMap.add(varInSequence.name, varInSequence.getVariable());
        inputMap.add(varInRectangleWidth.name, varInRectangleWidth.getVariable());
        inputMap.add(varInRectangleHeight.name, varInRectangleHeight.getVariable());
        inputMap.add(varInNumberOfRectangles.name, varInNumberOfRectangles.getVariable());
        inputMap.add(varInLimitRectanglesToROIs.name, varInLimitRectanglesToROIs.getVariable());
    }

    private VarROIArray varOutRectangles;

    @Override
    public void declareOutput(VarList outputMap)
    {
        varOutRectangles = new VarROIArray("Random rectangles");
        outputMap.add(varOutRectangles.getName(), varOutRectangles);
    }

    @Override
    protected void execute()
    {
        readParameters();
        try
        {
            generateRandomRectangles();
        }
        catch (Exception e)
        {
            throw new IcyHandledException("Error generating random rectangles: " + e.getMessage(), e);
        }
        setOutput();
    }

    private Sequence sequence;
    private Dimension rectangleSize;
    private Integer rectangleCount;
    private Boolean limitRectanglesToRois;
    private List<ROI2D> limitingRois;

    private void readParameters()
    {
        sequence = varInSequence.getValue(true);
        rectangleSize = new Dimension(varInRectangleWidth.getValue(), varInRectangleHeight.getValue());
        rectangleCount = varInNumberOfRectangles.getValue();
        limitRectanglesToRois = varInLimitRectanglesToROIs.getValue();
        limitingRois = new ArrayList<>();
        List<ROI2D> sequenceRois = sequence.getROI2Ds();
        if (!limitRectanglesToRois || sequenceRois.isEmpty())
        {
            limitingRois.add(new ROI2DRectangle(new Rectangle(sequence.getWidth(), sequence.getHeight())));
        }
        else
        {

            limitingRois.addAll(sequenceRois);
        }
    }

    private List<ROI2DRectangle> rectangles;

    private void generateRandomRectangles() throws Exception
    {
        RandomRectangleGenerator rectGenerator = new RandomRectangleGenerator.Builder(rectangleSize, rectangleCount,
                limitingRois).build();
        rectangles = rectGenerator.call();
    }

    private void setOutput()
    {
        if (isHeadLess())
        {
            varOutRectangles.setValue(rectangles.stream().toArray(ROI[]::new));
        }
        else
        {
            sequence.addROIs(rectangles, true);
        }

    }

    @Override
    public void clean()
    {// Nothing to do here
    }

}
